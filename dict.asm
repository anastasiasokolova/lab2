global find_word
extern string_equals

%include "colon.inc"

global _start

section .text

find_word:
; rdi - указатель на нуль треминированную строку
; rsi - указатель на начало словаря
mov r8, rdi									
.loop:
	mov r9, rsi
	cmp rsi, 0 			
	je .exit_false 		; если список пустой(при первом вхождениии) или если слово не найдено - выход с 0
	mov rdi, r8			
	add rsi, 8			
	call string_equals	; сравниваем слова
	mov rsi, r9			
	cmp rax, 0 			
	jnz .exit_true		; если string_equals вернула 0, то слова совпали, сохраняем указатель на строку
	mov rsi, [rsi]		; иначе смотрим следующее слово
    jmp .loop
	
.exit_true:				
	mov rax, rsi
	ret 
	
.exit_false:
	mov rax, 0			
	ret