section .data
	message: db "not found", 0

global _start	
	
section .text
%include "colon.inc"
%include "words.inc"

extern read_word
extern find_word
extern string_length
extern print_string
extern print_newline

_start: 
	
	mov rsi, 255	
	sub rsp, 256	; выделяем место под буфер в стеке (дальше функция дописывает нуль терминатор, поэтому 255+1)
	
	mov rdi, rsp
	call read_word	; при успехе возвращает адрес буфера в rax, длину слова в rdx
	
	cmp rax, 0		
	je .false		; выходим, если нет слова
	mov rdi, rax	
	mov rsi, next	; сохраняем адрес последнего слова
	call find_word	; ищем слово, возвращает указатель на строрку
	cmp rax, 0		
	je .false		; выходим, если слово не найдено
	add rax, 8			
	mov r10, rax 	
	mov rdi, rax				
	call string_length	; принимает указатель на нуль-терминированную строку, возвращает её длину
	add r10, rax	; 
	inc r10			; сдвигаем указатель на начало метки с учетом нуль терминатора
	mov rdi, r10
	
	mov r11, 1		; дескриптор stdout
	call print_string	; принимает указатель на нуль-терминированную строку, выводит её в stdout
	
.exit:	
	call print_newline	; переводит строку (выводит символ с кодом 0xA)
	mov rax, 60 	; дескриптор exit
	syscall
	
.false:
	mov rdi, message	; если нет слова, сообщаем
	call string_length
	mov rsi, rax
	
	mov r11, 2		; дескриптор stderr
	call print_string
	jmp .exit