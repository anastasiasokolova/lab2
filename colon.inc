%define next 0
%macro colon 2 	;
    %2: 
        dq next 		; указатель на следующую строку
        db %1, 0
    %define next %2     ; адерс следующей строки
%endmacro