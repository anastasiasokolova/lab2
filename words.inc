%include "colon.inc"

colon "mercury", mercury
db "Ближайшая к Солнцу планета Солнечной системы, наименьшая из планет земной группы", 0 

colon "venus", venus
db "Вторая по удалённости от Солнца планета Солнечной системы, названа в честь древнеримской богини любви Венеры", 0

colon "earth", earth
db "третья по удалённости от Солнца планета Солнечной системы, единственное известное человеку в настоящее время тело, населённое живыми организмами.", 0

colon "mars", mars
db "четвёртая по удалённости от Солнца и седьмая по размерам планета Солнечной системы, названа в честь Марса — древнеримского бога войны", 0